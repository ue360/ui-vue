export default [{
	path: "/",
	name: "首页",
	component: () => import("@/layouts/BasicLayout"),
	redirect: '/dashboard',
	children: [{
		path: "/dashboard",
		name: "控制台",
		icon: "home",
		component: () => import("@/pages/dashboard")
	}, {
		path: "/user",
		name: "用户",
		icon: "user",
		component: () => import("@/pages/user")
	}]
}]
