import constantRouterMap from './router.constant'
import asyncRouterMap from './router.async'

export {
	constantRouterMap,
	asyncRouterMap
}