export default [{
	path: "/login",
	component: () => import("@/layouts/LoginLayout"),
	children: [{
		path: "/login",
		component: () => import("@/pages/login")
	}]
}]