const path = require("path");
// const mocker = require('webpack-api-mocker')
// const glob = require('glob')
const mock = require("umi-mock");

function resolve(dir) {
  return path.join(__dirname, dir);
}

// const mock = glob.sync("mock/**/*.js")
// 	.map(p => path.join(__dirname, p))
// 	.reduce((memo, mockFile) => {
// 		try {
// 			const m = require(mockFile);
// 			memo = {
// 				...memo,
// 				...(m.default || m),
// 			};
// 			return memo;
// 		} catch (e) {
// 			throw new Error(e.stack);
// 		}
// 	}, {})

const cwd = process.cwd();
const absPagesPath = path.join(cwd, "src/pages");

module.exports = {
  chainWebpack: config => {
    config.resolve.alias.set("@$", resolve("src"));
  },
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true
      }
    }
  },
  devServer: {
    port: process.env.PORT,
    before: app => {
      if (process.env.NODE_ENV === "development") {
        app.use(
          mock.createMiddleware({
            cwd,
            errors: [],
            absPagesPath,
            absSrcPath: path.join(absPagesPath, "../"),
            watch: true
          })
        );
      }
    }
    // proxy: {
    // 	"/api": {
    // 		"target": "http://localhost:8080/portal/web-debug/api/",
    // 		"changeOrigin": true,
    // 		"pathRewrite": {"^/api": ""}
    // 	}
    // }
  },
  lintOnSave: false,
  productionSourceMap: false
};
