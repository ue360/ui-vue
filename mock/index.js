module.exports = {
    [`POST /api/login`] (req, res) {
     res.status(200).json({
         code: 200,
     })
    },

    [`GET /api/user`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             name: 'admin'
         }
     })
    },

    [`POST /api/users`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             total: 300,
             fields: {
                 types: [{
                     id: 1,
                     name: '启用'
                 }, {
                     id: 0,
                     name: '停用'
                 }]
             },
             data: [{
                 id: 1,
                 name: 'admin',
                 age: 28,
                 type: 1
             }, {
                 id: 2,
                 name: 'kelvin',
                 age: 36,
                 type: 0
             }]
         }
     })
    },

    [`GET /api/role`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             name: '内部角色'
         }
     })
    },

    [`POST /api/roles`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             total: 300,
             data: [{
                 id: 1,
                 name: '角色名称一'
             }, {
                 id: 2,
                 name: '角色名称二'
             }]
         }
     })
    },

    [`GET /api/roles`] (req, res) {
     res.status(200).json({
         code: 200,
         data: [{
             id: 1,
             name: '角色名称一'
         }, {
             id: 2,
             name: '角色名称二'
         }]
     })
    },

    [`GET /api/unit`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             id: 1,
             name: '测试部门一'
         }
     })
    },

    [`POST /api/units`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             total: 300,
             data: [{
                 id: 1,
                 name: '测试部门一',
                 type: 1
             }, {
                 id: 2,
                 name: '测试部门二',
                 type: 0
             }]
         }
     })
    },

    [`GET /api/units`] (req, res) {
     res.status(200).json({
         code: 200,
         data: [{
             id: 1,
             name: '测试部门一',
             type: 1
         }, {
             id: 2,
             name: '测试部门二',
             type: 0
         }]
     })
    },

    [`GET /api/task`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             id: 1,
             name: '计划任务'
         }
     })
    },

    [`POST /api/tasks`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             total: 300,
             data: [{
                 id: 1,
                 name: '计划任务一',
                 type: 1
             }, {
                 id: 2,
                 name: '计划任务二',
                 type: 0
             }]
         }
     })
    },

    [`GET /api/folder`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             name: '文件夹名称'
         }
     })
    },

    [`POST /api/folders`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             total: 200,
             data: [{
                 id: 1,
                 name: '文件夹名一'
             }, {
                 id: 2,
                 name: '这是传说中的测试'
             }, {
                 id: 3,
                 name: '文件夹名一'
             }, {
                 id: 4,
                 name: '这是传说中的测试'
             }, {
                 id: 5,
                 name: '文件夹名一'
             }, {
                 id: 6,
                 name: '这是传说中的测试'
             }, {
                 id: 7,
                 name: '文件夹名一'
             }, {
                 id: 8,
                 name: '这是传说中的测试'
             }]
         }
     })
    },

    [`GET /api/file`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             name: '文件名称'
         }
     })
    },

    [`POST /api/files`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             total: 200,
             data: [{
                 id: 1,
                 name: '只是一张图片'
             }, {
                 id: 2,
                 name: '这是传说中的测试'
             }]
         }
     })
    },

    [`GET /api/field`] (req, res) {
        res.status(200).json({
            code: 200,
            data: {
                name: '前置机名称'
            }
        })
    },

    [`POST /api/fields`] (req, res) {
        res.status(200).json({
            code: 200,
            data: {
                total: 200,
                data: [{
                    id: 1,
                    name: '前置机测试数据'
                }, {
                    id: 2,
                    name: '这是传说中的测试'
                }]
            }
        })
    },

    [`POST /api/analysis`] (req, res) {
        res.status(200).json({
            code: 200,
            data: {
                total: 200,
                data: [{
                    id: 1,
                    name: '只是一张图片'
                }, {
                    id: 2,
                    name: '这是传说中的测试'
                }]
            }
        })
    },

    [`GET /api/modules`] (req, res) {
     res.status(200).json({
         code: 200,
         data: [{"id":1,"text":"部门管理","items":[{"id":2,"text":"部门列表","parentId":1,"items":[],"moduleName":"ORG_VIEW","moduleUrl":"/api/units","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":3,"text":"新增部门","parentId":1,"items":[],"moduleName":"ORG_ADD","moduleUrl":"/api/unit/add","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":4,"text":"修改部门","parentId":1,"items":[],"moduleName":"ORG_EDIT","moduleUrl":"/api/unit/update","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":5,"text":"删除部门","parentId":1,"items":[],"moduleName":"ORG_DEL","moduleUrl":"/api/unit/delete","checked":false,"enabled":true,"expanded":true,"hasChildren":false}],"moduleName":"ORG_MANAGER","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":true},{"id":6,"text":"用户管理","items":[{"id":7,"text":"用户列表","parentId":6,"items":[],"moduleName":"USER_VIEW","moduleUrl":"/api/users","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":8,"text":"新增用户","parentId":6,"items":[],"moduleName":"USER_ADD","moduleUrl":"/api/user/add","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":9,"text":"修改用户","parentId":6,"items":[],"moduleName":"USER_EDIT","moduleUrl":"/api/user/update","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":10,"text":"删除用户","parentId":6,"items":[],"moduleName":"USER_DEL","moduleUrl":"/api/user/delete","checked":false,"enabled":true,"expanded":true,"hasChildren":false}],"moduleName":"USER_MANAGER","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":true},{"id":11,"text":"角色管理","items":[{"id":12,"text":"角色列表","parentId":11,"items":[],"moduleName":"ROLE_VIEW","moduleUrl":"/api/roles","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":13,"text":"新增角色","parentId":11,"items":[],"moduleName":"ROLE_ADD","moduleUrl":"/api/role/add","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":14,"text":"修改角色","parentId":11,"items":[],"moduleName":"ROLE_EDIT","moduleUrl":"/api/role/update","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":15,"text":"删除角色","parentId":11,"items":[],"moduleName":"ROLE_DEL","moduleUrl":"/api/role/delete","checked":false,"enabled":true,"expanded":true,"hasChildren":false}],"moduleName":"ROLE_MANAGER","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":true},{"id":16,"text":"文件夹管理","items":[{"id":17,"text":"文件夹列表","parentId":16,"items":[],"moduleName":"FOLDER_VIEW","moduleUrl":"/api/folders","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":18,"text":"所有文件夹列表","parentId":16,"items":[],"moduleName":"FOLDER_VIEWS","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":19,"text":"新增文件夹","parentId":16,"items":[],"moduleName":"FOLDER_ADD","moduleUrl":"/api/folder/add","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":20,"text":"修改文件夹","parentId":16,"items":[],"moduleName":"FOLDER_EDIT","moduleUrl":"/api/folder/update","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":21,"text":"删除文件夹","parentId":16,"items":[],"moduleName":"FOLDER_DEL","moduleUrl":"/api/folder/delete","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":22,"text":"文件管理","parentId":16,"items":[{"id":23,"text":"文件列表","parentId":22,"items":[],"moduleName":"FILE_VIEW","moduleUrl":"/api/files","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":24,"text":"文件上传","parentId":22,"items":[],"moduleName":"FILE_ADD","moduleUrl":"/file/upload","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":25,"text":"文件重命名","parentId":22,"items":[],"moduleName":"FILE_EDIT","moduleUrl":"/api/file/update","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":26,"text":"文件删除","parentId":22,"items":[],"moduleName":"FILE_DEL","moduleUrl":"/api/file/delete","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":27,"text":"重新上传","parentId":22,"items":[],"moduleName":"FILE_UPLOAD","moduleUrl":"/file/update","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":28,"text":"文件下载","parentId":22,"items":[],"moduleName":"FILE_DOWNLOAD","moduleUrl":"/file/download","checked":false,"enabled":true,"expanded":true,"hasChildren":false}],"moduleName":"FILE_MANAGER","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":true}],"moduleName":"FOLDER_MANAGER","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":true},{"id":29,"text":"周工作计划","items":[{"id":30,"text":"计划列表","parentId":29,"items":[],"moduleName":"WEEK_VIEW","moduleUrl":"/api/week/tasks","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":31,"text":"所有部门计划列表","parentId":29,"items":[],"moduleName":"WEEK_VIEWS","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":32,"text":"新建计划","parentId":29,"items":[],"moduleName":"WEEK_ADD","moduleUrl":"/api/week/task/add","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":33,"text":"修改计划","parentId":29,"items":[],"moduleName":"WEEK_EDIT","moduleUrl":"/api/week/task/update","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":34,"text":"删除计划","parentId":29,"items":[],"moduleName":"WEEK_DEL","moduleUrl":"/api/week/task/delete","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":35,"text":"督办","parentId":29,"items":[],"moduleName":"WEEK_ACTION","moduleUrl":"/api/week/task/save","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":36,"text":"审核","parentId":29,"items":[],"moduleName":"WEEK_REVIEW","moduleUrl":"/api/week/task/pass","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":37,"text":"导出计划","parentId":29,"items":[],"moduleName":"WEEK_EXPORT","moduleUrl":"/api/week/task/exports","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":38,"text":"查看详情","parentId":29,"items":[],"moduleName":"WEEK_PREVIEW","moduleUrl":"/api/week/task","checked":false,"enabled":true,"expanded":true,"hasChildren":false}],"moduleName":"WEEK_MANAGER","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":true},{"id":39,"text":"周议题","items":[{"id":40,"text":"议题列表","parentId":39,"items":[],"moduleName":"ISSUE_VIEW","moduleUrl":"/api/issue/tasks","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":41,"text":"所有部门议题列表","parentId":39,"items":[],"moduleName":"ISSUE_VIEWS","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":42,"text":"新建议题","parentId":39,"items":[],"moduleName":"ISSUE_ADD","moduleUrl":"/api/issue/task/add","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":43,"text":"修改议题","parentId":39,"items":[],"moduleName":"ISSUE_EDIT","moduleUrl":"/api/issue/task/update","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":44,"text":"删除议题","parentId":39,"items":[],"moduleName":"ISSUE_DEL","moduleUrl":"/api/issue/task/delete","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":45,"text":"督办","parentId":39,"items":[],"moduleName":"ISSUE_ACTION","moduleUrl":"/api/issue/task/save","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":46,"text":"审核","parentId":39,"items":[],"moduleName":"ISSUE_REVIEW","moduleUrl":"/api/issue/task/pass","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":47,"text":"导出议题","parentId":39,"items":[],"moduleName":"ISSUE_EXPORT","moduleUrl":"/api/issue/task/exports","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":48,"text":"查看详情","parentId":39,"items":[],"moduleName":"ISSUE_PREVIEW","moduleUrl":"/api/issue/task","checked":false,"enabled":true,"expanded":true,"hasChildren":false}],"moduleName":"ISSUE_MANAGER","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":true},{"id":49,"text":"年度工作计划","items":[{"id":50,"text":"计划列表","parentId":49,"items":[],"moduleName":"YEAR_VIEW","moduleUrl":"/api/year/tasks","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":51,"text":"所有部门计划列表","parentId":49,"items":[],"moduleName":"YEAR_VIEWS","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":52,"text":"新建计划","parentId":49,"items":[],"moduleName":"YEAR_ADD","moduleUrl":"/api/year/task/add","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":53,"text":"删除计划","parentId":49,"items":[],"moduleName":"YEAR_DEL","moduleUrl":"/api/year/task/delete","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":54,"text":"督办","parentId":49,"items":[],"moduleName":"YEAR_ACTION","moduleUrl":"/api/year/task/save","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":55,"text":"审核","parentId":49,"items":[],"moduleName":"YEAR_REVIEW","moduleUrl":"/api/year/task/pass","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":56,"text":"导出计划","parentId":49,"items":[],"moduleName":"YEAR_EXPORT","moduleUrl":"/api/year/task/exports","checked":false,"enabled":true,"expanded":true,"hasChildren":false},{"id":57,"text":"查看详情","parentId":49,"items":[],"moduleName":"YEAR_PREVIEW","moduleUrl":"/api/year/task","checked":false,"enabled":true,"expanded":true,"hasChildren":false}],"moduleName":"YEAR_MANAGER","moduleUrl":"","checked":false,"enabled":true,"expanded":true,"hasChildren":true},{"id":58,"text":"操作日志","items":[],"moduleName":"LOG_VIEW","moduleUrl":"/api/logs","checked":false,"enabled":true,"expanded":true,"hasChildren":false}]
     })
    },

    [`POST /api/logs`] (req, res) {
     res.status(200).json({
         code: 200,
         data: {
             total: 200,
             data: [{
                 id: 1,
                 name: 'admin登录成功'
             }, {
                 id: 2,
                 name: '这是传说中的测试'
             }]
         }
     })
    },
}