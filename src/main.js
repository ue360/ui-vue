import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./icons";
// 权限守卫
import "./authorized";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
