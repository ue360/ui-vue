import { Menu } from 'ant-design-vue'

export default {
	name: 'SideMenu',
	props: {
		menu: {
			type: Array,
			required: true
		},
		system: {
			type: Boolean,
			required: false,
			default: false
		}
	},
	data() {
		return {
			selectedKeys: []
		}
	},
	watch: {
		$route() {
			this.updateMenu()
		}
	},
	methods: {
		getMenu(data) {
			return (data || []).filter(item => !item.hide && (item.system||false) === this.system).map(item => this.getSubMenuOrItem(item))
		},
		getSubMenuOrItem(item) {
			return <Menu.Item { ...{ key: item.path } }><router-link class="ico-nav" { ...{ props: { to: item.path } } }><icon { ...{ props: { name: item.icon } } } /><span class="ico-text">{item.name}</span></router-link></Menu.Item>
		},
		updateMenu() {
			const routes = this.$route.matched.concat()
			this.selectedKeys = [routes.pop().path]
		}
	},
	mounted() {
		this.updateMenu()
	},
	render() {
		const {
			menu
		} = this

		return (
			<Menu prefixCls="x-menu" { ...{ props: { selectedKeys: this.selectedKeys }} }>
				{this.getMenu(menu)}
			</Menu>
		)
	}
}