import Vue from 'vue'
import PropTypes from 'ant-design-vue/es/_util/vue-types';
// import { Table } from 'ant-design-vue'
import { PaginationProps as getPaginationProps } from '@/components/Pagination';
import Table from '@/components/Table'

const {
	current,
	defaultCurrent,
	...PaginationProps
} = getPaginationProps();

Vue.use(Table)

export default {
	name: 'UiTable',
	data() {
		return {
			
		}
	},
	props: {
		...Table.props,
		pagination: PropTypes.oneOfType([
			PropTypes.shape({
				defaultPageNumber: PropTypes.number,
  				pageNumber: PropTypes.number,
				...PaginationProps
			}).loose,
			PropTypes.bool
		]),
		data: {
			type: Array,
			required: true
		},
		loading: {
			type: Boolean,
			required: false
		}
	},
	methods: {
		
	},
	render() {
		const props = {
			prefixCls: 'x-grid',
			scroll: {
				x: true, 
				y: true
			},
			dataSource: this.data,
			loading: {
				prefixCls: 'x-loading',
				spinning: this.loading
			}
		}
		Object.keys(Table.props).forEach(key => {
			if (this[key] && !props[key]) {
				props[key] = this[key]
			}
		})
		const {
			pagination
		} = this

		if (pagination) {
			const {
				pageNumber,
				defaultPageNumber,
				...restPaginationProps
			} = pagination

			props.pagination = {
				current: pageNumber,
				defaultCurrent: defaultPageNumber,
				...restPaginationProps
			}
		}
		return (
			<a-table 
				{ ...{ props } }
			>
				
			</a-table>
		)
	}
}