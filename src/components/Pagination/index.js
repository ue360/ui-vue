import PropTypes from 'ant-design-vue/es/_util/vue-types';
import VcSelect from 'ant-design-vue/es/select';
import MiniSelect from 'ant-design-vue/es/pagination/MiniSelect';
import LocaleReceiver from 'ant-design-vue/es/locale-provider/LocaleReceiver';
import { getOptionProps } from 'ant-design-vue/es/_util/props-util';
import VcPagination from './Pagination';
import Icon from 'ant-design-vue/es/icon';

export const PaginationProps = () => ({
  total: PropTypes.number,
  defaultCurrent: PropTypes.number,
  current: PropTypes.number,
  defaultPageSize: PropTypes.number,
  pageSize: PropTypes.number,
  hideOnSinglePage: PropTypes.bool,
  showSizeChanger: PropTypes.bool,
  pageSizeOptions: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  buildOptionText: PropTypes.func,
  showSizeChange: PropTypes.func,
  showQuickJumper: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]).def(false),
  showTotal: PropTypes.any,
  size: PropTypes.string,
  simple: PropTypes.bool,
  locale: PropTypes.object,
  prefixCls: PropTypes.string,
  selectPrefixCls: PropTypes.string,
  itemRender: PropTypes.any,
  role: PropTypes.string,
});

export const PaginationConfig = () => ({
  ...PaginationProps(),
  position: PropTypes.oneOf(['top', 'bottom', 'both']),
});

export default {
  name: 'APagination',
  model: {
    prop: 'current',
    event: 'change.current',
  },
  props: {
    ...PaginationProps(),
    prefixCls: PropTypes.string.def('x-page'),
    selectPrefixCls: PropTypes.string.def('x-page-select'),
    simple: PropTypes.bool.def(true)
  },
  methods: {
    getIconsProps() {
      const { prefixCls } = this.$props;
      const firstIcon = (
          <Icon type="vertical-right" />
      );
      const prevIcon = (
            <Icon type="left" />
      );
      const nextIcon = (
            <Icon type="right" />
      );
      const lastIcon = (
            <Icon type="vertical-left" />
      );
      return {
        firstIcon,
        prevIcon,
        nextIcon,
        lastIcon,
        jumpPrevIcon: prevIcon,
        jumpNextIcon: nextIcon
      };
    }
  },
  render() {
    const { buildOptionText, size, ...restProps } = getOptionProps(this);
    const isSmall = size === 'small';
    const paginationProps = {
      props: {
        ...restProps,
        ...this.getIconsProps(),
        selectComponentClass: isSmall ? MiniSelect : VcSelect,
        buildOptionText: buildOptionText || this.$scopedSlots.buildOptionText
      },
      class: {
        mini: isSmall,
      },
      on: this.$listeners,
    };
    return (
      <VcPagination {...paginationProps} />
    );
  }
};
