import user from '@/api/user'
import { asyncRouterMap as routes } from "../../config/router.config";
import { formatterData } from '@/utils'
import { resetRouter } from '@/router'

export default {
	state: {
		user: {},
		menus: []
	},
	mutations: {
		userInfo(state, payload) {
			state.user = {
				...payload
			}
		},
		routeList(state, payload) {
			state.menus = payload
		}
	},
	actions: {
		async getUserInfo({ commit }) {
			const data = await user.info()
			commit('userInfo', data)
		},
		async asyncRoutes({ state, commit }) {
			const {
				user: {
					authorities
				}
			} = state
			const data = await formatterData(routes, authorities)

			commit('routeList', data)
		},
		async logout({ commit }) {
			await user.logout()

			commit('userInfo', {})
			commit('routeList', [])
			resetRouter()
		}
	}
}