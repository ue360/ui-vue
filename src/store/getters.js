const getters = {
	userInfo: state => state.app.user,
	routes: state => state.app.menus
}
export default getters