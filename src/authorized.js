import router from "./router";
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 去除加载（右上角转圈圈效果状态）
NProgress.configure({ showSpinner: false })

const whiteList = ['/login']

router.beforeEach(async (to, from, next) => {
	NProgress.start()
	const info = store.getters.userInfo

	// 第一次访问系统或者刷新页面
	if (Object.keys(info).length === 0 && !whiteList.includes(to.path)) {
		await store.dispatch('getUserInfo')
		await store.dispatch('asyncRoutes')
		router.addRoutes(store.getters.routes)
		next({ 
			...to,
			replace: true
		})
	} else {
		next()
	}
});

router.afterEach(() => {
	NProgress.done()
});