import Vue from "vue";
import Router from "vue-router";
import { constantRouterMap as routes } from "../../config/router.config";

Vue.use(Router);

const base = {
	mode: "history",
	base: process.env.BASE_URL,
	scrollBehavior: () => ({ y: 0 })
}

const createRouter = () => new Router({
  ...base,
  routes
})

const router = createRouter()

export const resetRouter = () => {
	const newRouter = createRouter()
  	router.matcher = newRouter.matcher
}

export default router